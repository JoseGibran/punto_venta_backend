<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

class AuthController extends Controller
{
    public function validator(Request $request){
    	$user = json_decode($request->all()["user"], true);


        $result = DB::connection('emps')->table('users')
            ->where('user', $user["user"])
            ->first();
        $pass = "";
        for($i = 0; $i < strlen($result->password); $i = $i + 3){
           $pass = $pass . chr(substr($result->password, $i, 3));
        }

        if($result){
            if($pass == $user["password"]){
                $caja = DB::table('cajas')
                    ->where('user', $result->user)
                    ->first();
                if(!is_null($caja)){
                    $turno = DB::table('cajas_turnos')
                        ->where('caja', $caja->caja)
                        ->where('user', $user["user"])
                        ->first();
                    $tipoDocumento = DB::table('tipos_docs')
                        ->where('tipo', $caja->tipo)
                        ->first();
                    $bodega = DB::table('productos_bodega')
                        ->where('bodega', $caja->bodega)
                        ->first();
                    $response = [
                        'user' => $result, 
                        'caja' => $caja, 
                        'turno'=>  $turno, 
                        'document_type' => $tipoDocumento,
                        'cellar' => $bodega
                    ];
                    return json_encode($response);
                }else{
                    return json_encode(false);
                }              
            }else{
                return json_encode(false);
            }
        }else{  
            return json_encode(false);
        }
    }
    public function openTurno($id){
        $caja = DB::table('cajas')
            ->where('caja', $id)
            ->first();
        $turnoCount = DB::table('cajas_turnos')
            ->where('caja', $id)
            ->select('caja')
            ->count() + 1;
        DB::table('cajas_turnos')
            ->insert([
                    'turno' => $turnoCount,
                    'caja' => $id,
                    'fecha' => date("Y-m-d"),
                    'user' => $caja->user,
                    'status' => 0
            ]);
        DB::table('cajas')
            ->where('caja', $id)
            ->update([
                'close' => 0,
                'turno' => $turnoCount
            ]);

        return json_encode(true);
    }

    public function user($id){
        $user = DB::connection("emps")->table('users')
            ->where('idusers', $id)
            ->first();
        return json_encode($user);
    }
    public function caja($id){
        $response = array();
        $caja = DB::table('cajas')
            ->where('idCajas', $id)
            ->first();


        if($caja != null){
             $tipoDocumento = DB::table('tipos_docs')
            ->where('tipo', $caja->tipo)
            ->first();
            $bodega = DB::table('productos_bodega')
                ->where('bodega', $caja->bodega)
                ->first();
            $response["caja"] = $caja;
            $response["document_type"] = $tipoDocumento;
            $response["sellar"] = $bodega;
             return json_encode($response);
        }

        return json_encode(false);
       
       
    }

}
//CIERTE
/*
Cajas = 1
Imprime  el corte de caja (Resumen de ventas) 
Integracion forma de pago
*/
//VENTAS

