<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use PDF;
use DB;

class BillController extends Controller
{

    public function store(Request $request){
    	$path = app_path("..") . '/resources/bills/';
    	
    	$bill = json_decode($request -> all()["bill"], true);

    	$customer = $bill["customer"];
    	$products = $bill["products"];
        $sellar = $bill["sellar"];
        $document_type = $bill["document_type"];
        $caja = $bill["caja"];

        $ID = DB::table('tipos_docs')
            ->where('tipo', $caja["tipo"])
            ->first() -> correlativo;
        $serie = DB::table('tipos_docs')
            ->where('tipo', $caja["tipo"])
            ->first() -> serie;
        $tasa = DB::table('tipos_docs')
            ->where('tipo', $caja["tipo"])
            ->first() -> tasa;
        
        //GRABADO ES EL TOTAL SIN IVA
        //IVA 
        //BIENES EN docu_enc

        //APARTE HAY QUE HACER OTRA TABLA LLAMADA docu_ctacte




        
		$subTotal = 0;
		foreach($products as $product){
            $exis = DB::table('productos_exis')->where('producto', $product["product"])->first();
            $newExis = intval($exis->existencia) - intval($product["count"]);
            DB::table('productos_exis')
                ->where('idproductos_exis', $exis->idproductos_exis)
                ->update([
                    'existencia' => $newExis
            ]);



            $result = DB::table('compras_det')
                ->insert([
                    "grupo_cta" => "FACTURA",
                    "grupo_tip" => $document_type["tipo"],
                    "grupo_num" => $ID,
                    "cantidad" => $product["count"],
                    "unitario" => $product["price"]
                ]);
            


			$subTotal += $product['price'] * $product["count"];
		}
		$IVA = $subTotal * ($tasa / 100);
		$total = $subTotal + $IVA;
        $grabado = number_format($total / 1.12, 2);


         DB::table('docu_enc')->insert([
            "grupo_cta" => "INGRESOS",
            "grupo_tip" => "200",
            "ctacte" => "CONTADO",
            "grupo_num" => ltrim(date("m"), "0") . date("Y"),
            "tipo_doc" => $document_type["tipo"],
            "serie" => $serie,
            "numero" => $ID,
            "fechadoc" => DB::raw('now()'),
            "nit" => $customer["nit"],
            "iva" => $IVA
        ]); 
        DB::table('compras_enc')->insert([
            "grupo_cta" => "FACTURA",
            "grupo_tip" => $document_type["tipo"],
            "grupo_num" => $ID,
            "nit" => $customer["nit"],
            "grupo_fec" =>  DB::raw('now()')
        ]);
        DB::table('tipos_docs')
            ->where('tipo', $caja["tipo"])
            ->update([
                'correlativo' => intval($ID) + 1
            ]);
        DB::table('docu_ctacte')->insert([
            "grupo_cta" => "INGRESOS",
            "grupo_tip" => "200",
            "ctacte" => "CONTADO",
            "grupo_num" => ltrim(date("m"), "0") . date("Y"),
            "tipo_doc" => $document_type["tipo"],
            "serie" => $serie,
            "numero" => $ID,
            "fechadoc" => DB::raw('now()'),
            "nit" => $customer["nit"],
            "cargo" => $total,
            "saldo" => $total
        ]);
       

    	//Save PDF
    	$pdf = PDF::loadView('bill', [
    		"ID" => $ID,
    		"customer" =>  $customer, 
    		"products" => $products, 
    		"subTotal" => $subTotal,
    		"IVA" => $IVA,
    		"total" => $total,
    	]);
    	$pdf->save( $path . $ID . '.pdf');
    	return json_encode($ID);
    }

    public function ticket(Request $request){
        $path = app_path("..") . '/resources/tickets/';
        $bill = json_decode($request -> all()["bill"], true);

        $customer = $bill["customer"];
        $products = $bill["products"];
        $sellar = $bill["sellar"];
        $caja = $bill["caja"];

        if(!isset($bill["document_type"])){
            return json_encode(array("error" => "No se setio un tipo de documento en la caja, agregelo e inicie sesion nuevamente"));
        }

        $document_type = $bill["document_type"];
        
        $tasa = "";
        $serie = "";

        if(is_null($caja["tipo"])){    return json_encode(array("error" =>
        "Falta el tipo de caja")); }else{
        if(DB::table('tipos_docs')->where('tipo', $caja["tipo"])->first()){
        $tasa = DB::table('tipos_docs') ->where('tipo',$caja["tipo"])
        ->first() -> tasa;     $serie = DB::table('tipos_docs')
        ->where('tipo',$caja["tipo"]) ->first() -> serie;     }else{ return
        json_encode(array("error" => "No se encontro ningun tipo de
        documento"));     } }


    
        $ID = DB::table('tipos_docs') ->where('tipo', $caja["tipo"]) ->first() -> correlativo;


        $subTotal = 0;

        foreach($products as $product){
            //AQUI actualizamos la existencia;

            /*$exis = DB::table('productos_exis')->where('producto', $product["product"])->first();

            $newExis = intval($exis->existencia) - intval($product["count"]);
            DB::table('productos_exis')
                ->where('idproductos_exis', $exis->idproductos_exis)
                ->update([
                    'existencia' => $newExis
            ]);*/

            //AQUI ingresamos al detalle de productos
            DB::table('compras_det')
            ->insert([
                "producto" => $product["product"],
                "grupo_cta" => "FACTURA",
                "grupo_tip" => $document_type["tipo"],
                "grupo_num" => $ID,
                "cantidad" => $product["count"],
                "unitario" => $product["price"],
                "grupo_fec" => date('Y-m-d H:i:s'),
                "importe" => $product['price'],
            ]);
            $subTotal += $product['price'] * $product["count"];
        }
        $IVA = $subTotal * ($tasa / 100);
        $total = $subTotal + $IVA;
        $grabado = number_format($total / 1.12, 2);




        //TABLAS PARA FACTURACION
         DB::table('docu_enc')->insert([
            "grupo_cta" => "INGRESOS",
            "grupo_tip" => "200",
            "ctacte" => "CONTADO",
            "grupo_num" => ltrim(date("m"), "0") . date("Y"),
            "tipo_doc" => $document_type["tipo"],
            "serie" => $serie,
            "numero" => $ID,
            "fechadoc" => DB::raw('now()'),
            "nit" => $customer["nit"],
            "iva" => $IVA
        ]); 
        DB::table('compras_enc')->insert([
            "grupo_cta" => "FACTURA",
            "grupo_tip" => $document_type["tipo"],
            "grupo_num" => $ID,
            "nit" => $customer["nit"],
            "grupo_fec" =>  DB::raw('now()')
        ]);
        DB::table('tipos_docs')
            ->where('tipo', $caja["tipo"])
            ->update([
                'correlativo' => intval($ID) + 1
            ]);
        DB::table('docu_ctacte')->insert([
            "grupo_cta" => "INGRESOS",
            "grupo_tip" => "200",
            "ctacte" => "CONTADO",
            "grupo_num" => ltrim(date("m"), "0") . date("Y"),
            "tipo_doc" => $document_type["tipo"],
            "serie" => $serie,
            "numero" => $ID,
            "fechadoc" => DB::raw('now()'),
            "nit" => $customer["nit"],
            "cargo" => $total,
            "saldo" => $total
        ]);

        DB::table('cajas')
        ->where('idCajas', $caja["idCajas"])
        ->update([
            'factura' => $ID
        ]);
       




        return json_encode(true);
    }
}
