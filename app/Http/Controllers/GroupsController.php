<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class GroupsController extends Controller
{
    public function index(){
    	$groups = DB::table('productos_clasi')
    		->select('idproductos_clasi as ID')
    		->addSelect('clasi as description')
    		->get();
    	return json_encode($groups);
    }

    public function image($id, Request $request){
    	$path = app_path("..") . '/resources/img/groups/';
    	$newData = json_decode($request -> all()["group"], true);

    	//Save Image
    	$img = $newData["img"];
        $imgFile = base64_decode(
            preg_replace('#^data:image/\w+;base64,#i', '', $img)
        );
        $imgName = $id . ".png";
        file_put_contents($path . $imgName, $imgFile);
        return json_encode(true);
    }
}
