<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
	
use DB;

class ProductController extends Controller
{
	public function index(){
		$products = DB::table('productos')
			->select('idproductos as ID')
			->addSelect('producto as product')
			->addSelect('descrip as description')
			->addSelect('simp_val as price')
			->addSelect('clasi as group')
			->get();
		return json_encode($products);
	}
	public function group($id, $caja){


		$products = DB::table('productos')
			->select('idproductos as ID')
			->join('productos_lp_deta', 'productos.producto', '=', 'productos_lp_deta.producto')
			->addSelect('productos.producto as product')
			->addSelect('productos.descrip as description')
			->addSelect('productos_lp_deta.precio as price')
			->addSelect('productos.clasi as group')
			->where('productos.clasi', $id)
			->get();
		return json_encode($products);
	}
	public function image($id, Request $request){
    	$path = app_path("..") . '/resources/img/products/';
    	$newData = json_decode($request -> all()["product"], true);
    	//Save Image
    	$img = $newData["img"];
        $imgFile = base64_decode(
            preg_replace('#^data:image/\w+;base64,#i', '', $img)
        );
        $imgName = $id . ".png";
        file_put_contents($path . $imgName, $imgFile);
        return json_encode(true);
    }
}
