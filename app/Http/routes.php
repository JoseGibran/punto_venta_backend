<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('bill', function(){
	return view('bill');
	//Enable GD extension PHP
});
Route::get('ticket', function(){
	return view('ticket');
});




Route::group(['middleware' => 'cors'], function () {
	
	Route::post('auth' , ['as' => 'auth.validator', 'uses' => 'AuthController@validator']);

	Route::get('openturno/{id}', ['as' => 'auth.openturno', 'uses' => 'AuthController@openTurno']);


	Route::group(['prefix' => 'api'], function () {
		Route::resource('product', 'ProductController');
		Route::get('product/group/{id}/{caja}', ['as' => 'product.group', 'uses' => 'ProductController@group']);
		Route::post('product/image/{id}', ['as' => 'product.image', 'uses' => 'ProductController@image']);
		Route::resource('group', 'GroupsController');
		Route::post('group/image/{id}', ['as' => 'group.image', 'uses' => 'GroupsController@image']);

		Route::resource('bill', 'BillController');
		Route::post('bill/ticket/create', ['as' => 'bill.ticket', 'uses' => 'BillController@ticket']);

		Route::get('user/{id}', ['as' => 'auth.user', 'uses' => 'AuthController@user']);
		Route::get('caja/{id}', ['as' => 'auth.caja', 'uses' => 'AuthController@caja']);

   });
});