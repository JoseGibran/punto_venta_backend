<!DOCTYPE html>
<html>
<head>
	<title>FACTURA</title>
	<style type="text/css">
		.main{
			padding: 10px;
			width: 283.464566929px;
			border: 1px solid;
			font-size: 10px;
		}
		.header{
			text-align: center;
		}
		.bill{
			text-align: right;
			font-weight: 900;
		}
		table{
			width: 100%;
			text-align: left;
		}
		.footer{
			text-align: center;
		}

	</style>
</head>
<body>
	<div class="main"> 

		<div class="header">
			MEJORES INGREDIENTES, SOCIEDAD ANONIMA <br/>
			NIT: 8129533-2 <br/>
			3 CALLE 10-00 ZONA 4 CONDADO EL NARANJO CENTRO COMERCIAL "NARANJO MALL" TERCER NIVEL LOCAL FC-311 <br/>
			MIXCO, GUATEMALA <br/>
			TRIMESTRAL<br/>
			Resolucion: 2015-1-57-43438<br/>
			Fecha Resolucion: 03/11/2015<br/>
			Serie: NA<br/>
			Del: 1 a la: 50000<br/>
			Factura NO. 23782<br/>
			PIZZA PARA JOHN'S<br/>
			23 calle 10-00<br/>
			GUATEMELA DEPT GUATEMALA<br/>
		</div>
		<div class="customer">
			NIT: {{$customer["nit"]}}<br/>
			NOMBRE: {{$customer["nombre"]}}<br/>
			DIRECCION:{{$customer["direccion"]}}<br/>
		</div>
		<div class="bill">
			# {{$ID}}
		</div>
		<table>
		<tr>
			<th><h1 style="font-size: 44px">ALMUERZO</h1></th>
			<th>Valor</th>
		</tr>
		@foreach($products as $product)
			<tr>
				<td>{{$product["description"]}}</td>
				<td>{{number_format($product["price"], 2) }}</td>
			</tr>
		@endforeach
		<tr>
			<th>Subtotal</th>
			<td>{{number_format($subTotal, 2)}}</td>
		</tr>
		<tr>
			<th>Total</th>
			<td>{{number_format($total, 2)}}</td>
		</tr>
		
		<tr>
			<td>12.00% Total: {{$subTotal}} IVA: {{$IVA}}</td>
		</tr>
		</table>

		<div class="footer">
			Sujeto a Pagos <br/>
			Trimestrales <br />
			--Cuenta Cerrada-- <br/>
		</div>
	</div>
</body>
</html>


