<!DOCTYPE html>
<html>
<head>
	<title>TICKET</title>
	<style type="text/css">
		.main{
			text-align: center;
			width: 350px;
		}
		.title{
			font-size: 48px;
		}
		.footer{
			text-align: initial;
			font-size: 14px;
		}
	</style>
</head>
<body>
	<div class="main"> 	
		<h1 class="title">{{$products[0]["description"]}}</h1>

		@if (isset($products[1]))
			<h1>{{$products[1]["description"]}}</h1>
		@endif

		<div class="footer">
			Caja: {{$ID}}
			<span style="float: right;">{{date('d-m-Y , h:i:s A')}}</span>
		</div>
	</div>
</body>
</html>


